# 连锁公寓管理系统

#### 介绍
连锁公寓管理系统
工作台、公寓管理、房源管理、租赁查询、押金处理详情、收款详情、退款详情、水电费收款、费用管理、每日日报、企业管理、企业用户管理、角色管理、字典管理、用户管理

#### 开发技术
前端框架：VUE3 PINA VITE
后端框架：SPRING BOOT  JPA  QUERYDSL


#### 源码出售

前后端源码1万元起，包运行部署；
也支持二次开发。
开发培训另外沟通。

#### 联系方式

手机号：13916072467（微信同号）


![功能介绍](images2.pic.jpg)
![功能介绍](images1.png)
![功能介绍](images4.png)
![功能介绍](images3.png)